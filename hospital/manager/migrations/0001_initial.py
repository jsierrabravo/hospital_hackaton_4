# Generated by Django 4.0.5 on 2022-06-10 17:34

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Especialidad',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=50)),
                ('descripcion', models.TextField(max_length=500)),
                ('fecha_registro', models.DateTimeField(auto_now_add=True)),
                ('fecha_modificacion', models.DateTimeField(auto_now=True)),
                ('activo', models.BooleanField()),
                ('usuario_modificacion', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='usuario_modificacion_especialidad', to=settings.AUTH_USER_MODEL)),
                ('usuario_registro', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='usuario_registro_especialidad', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Medico',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombres', models.CharField(max_length=50)),
                ('apellidos', models.CharField(max_length=50)),
                ('dni', models.IntegerField()),
                ('direccion', models.CharField(max_length=150)),
                ('correo', models.EmailField(max_length=254)),
                ('telefono', models.IntegerField()),
                ('sexo', models.CharField(max_length=15)),
                ('num_colegiatura', models.IntegerField()),
                ('fecha_nacimiento', models.DateField()),
                ('fecha_registro', models.DateTimeField(auto_now_add=True)),
                ('fecha_modificacion', models.DateTimeField(auto_now=True)),
                ('activo', models.BooleanField()),
                ('usuario_modificacion', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='usuario_modificacion_medico', to=settings.AUTH_USER_MODEL)),
                ('usuario_registro', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='usuario_registro_medico', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Paciente',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombres', models.CharField(max_length=50)),
                ('apellidos', models.CharField(max_length=50)),
                ('dni', models.IntegerField()),
                ('direccion', models.CharField(max_length=50)),
                ('telefono', models.IntegerField()),
                ('sexo', models.CharField(max_length=15)),
                ('fecha_nacimiento', models.DateField()),
                ('fecha_registro', models.DateTimeField(auto_now_add=True)),
                ('fecha_modificacion', models.DateTimeField(auto_now=True)),
                ('activo', models.BooleanField()),
                ('usuario_modificacion', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='usuario_modificacion_paciente', to=settings.AUTH_USER_MODEL)),
                ('usuario_registro', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='usuario_registro_paciente', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='MedicoEspecialidad',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_registro', models.DateTimeField(auto_now_add=True)),
                ('fecha_modificacion', models.DateTimeField(auto_now=True)),
                ('activo', models.BooleanField()),
                ('id_especialidad', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='manager.especialidad')),
                ('id_medico', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='manager.medico')),
                ('usuario_modificacion', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='usuario_modificacion_medico_especialidad', to=settings.AUTH_USER_MODEL)),
                ('usuario_registro', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='usuario_registro_medico_especialidad', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Horario',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_atencion', models.DateTimeField()),
                ('inicio_atencion', models.DateTimeField()),
                ('fin_atencion', models.DateTimeField()),
                ('activo', models.BooleanField()),
                ('fecha_registro', models.DateTimeField(auto_now_add=True)),
                ('fecha_modificacion', models.DateTimeField(auto_now=True)),
                ('id_medico', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='manager.medico')),
                ('usuario_modificacion', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='usuario_modificacion_horario', to=settings.AUTH_USER_MODEL)),
                ('usuario_registro', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='usuario_registro_horario', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Cita',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_atencion', models.DateField()),
                ('inicio_atencion', models.DateField()),
                ('fin_atencion', models.DateField()),
                ('estado', models.BooleanField()),
                ('observaciones', models.CharField(max_length=50)),
                ('activo', models.BooleanField()),
                ('fecha_registro', models.DateTimeField(auto_now_add=True)),
                ('fecha_modificacion', models.DateTimeField(auto_now=True)),
                ('id_medico', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='manager.medico')),
                ('id_paciente', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='manager.paciente')),
                ('usuario_modificacion', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='usuario_modificacion_cita', to=settings.AUTH_USER_MODEL)),
                ('usuario_registro', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='usuario_registro_cita', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
