from django.db import models
from django.contrib.auth.models import User
from django.contrib import auth


# Create your models here.
class Especialidad(models.Model):
    nombre = models.CharField(max_length=50)
    descripcion = models.TextField(max_length=500)
    fecha_registro = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    usuario_registro = models.ForeignKey(User, on_delete=models.CASCADE, related_name="usuario_registro_especialidad", null=True, blank=True)
    usuario_modificacion = models.ForeignKey(User, on_delete=models.CASCADE, related_name="usuario_modificacion_especialidad", null=True, blank=True)
    activo = models.BooleanField()

    def __str__(self):
        return self.nombre


class Medico(models.Model):
    nombres = models.CharField(max_length=50)
    apellidos = models.CharField(max_length=50)
    dni = models.IntegerField()
    direccion = models.CharField(max_length=150)
    correo = models.EmailField()
    telefono = models.IntegerField()
    sexo = models.CharField(max_length=15)
    num_colegiatura = models.IntegerField()
    fecha_nacimiento = models.DateField()
    fecha_registro = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    usuario_registro = models.ForeignKey(User, on_delete=models.CASCADE, related_name="usuario_registro_medico", null=True, blank=True)
    usuario_modificacion = models.ForeignKey(User, on_delete=models.CASCADE, related_name="usuario_modificacion_medico", null=True, blank=True)
    activo = models.BooleanField()

    def __str__(self):
        return f"{self.nombres} {self.apellidos}"


class MedicoEspecialidad(models.Model):
    id_medico = models.ForeignKey(Medico, on_delete=models.CASCADE)
    id_especialidad = models.ForeignKey(Especialidad, on_delete=models.CASCADE)
    fecha_registro = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    usuario_registro = models.ForeignKey(User, on_delete=models.CASCADE, related_name="usuario_registro_medico_especialidad", null=True, blank=True)
    usuario_modificacion = models.ForeignKey(User, on_delete=models.CASCADE, related_name="usuario_modificacion_medico_especialidad", null=True, blank=True)
    activo = models.BooleanField()

    def __str__(self):
        return f"{self.id_medico} {self.id_especialidad}"


class Paciente(models.Model):
    nombres = models.CharField(max_length=50)
    apellidos = models.CharField(max_length=50)
    dni = models.IntegerField()
    direccion = models.CharField(max_length=50)
    telefono = models.IntegerField()
    sexo = models.CharField(max_length=15)
    fecha_nacimiento = models.DateField()
    fecha_registro = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    usuario_registro = models.ForeignKey(User, on_delete=models.CASCADE, related_name="usuario_registro_paciente", null=True, blank=True)
    usuario_modificacion = models.ForeignKey(User, on_delete=models.CASCADE, related_name="usuario_modificacion_paciente", null=True, blank=True)
    activo = models.BooleanField()

    def __str__(self):
        return f"{self.nombres} {self.apellidos}"


class Horario(models.Model):
    id_medico = models.ForeignKey(Medico, on_delete=models.CASCADE)
    fecha_atencion = models.DateTimeField()
    inicio_atencion = models.DateTimeField()
    fin_atencion = models.DateTimeField()
    activo = models.BooleanField()
    fecha_registro = models.DateTimeField(auto_now_add=True)
    usuario_registro = models.ForeignKey(User, on_delete=models.CASCADE, related_name="usuario_registro_horario", null=True, blank=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    usuario_modificacion = models.ForeignKey(User, on_delete=models.CASCADE, related_name="usuario_modificacion_horario", null=True, blank=True)

    def __str__(self):
        return f"{self.id_medico} {self.fecha_atencion}"


class Cita(models.Model):
    id_medico = models.ForeignKey(Medico, on_delete=models.CASCADE)
    id_paciente = models.ForeignKey(Paciente, on_delete=models.CASCADE)
    fecha_atencion = models.DateField()
    inicio_atencion = models.DateField()
    fin_atencion = models.DateField()
    estado = models.BooleanField()
    observaciones = models.CharField(max_length=50)
    activo = models.BooleanField()
    fecha_registro = models.DateTimeField(auto_now_add=True)
    usuario_registro = models.ForeignKey(User, on_delete=models.CASCADE, related_name="usuario_registro_cita", null=True, blank=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    usuario_modificacion = models.ForeignKey(User, on_delete=models.CASCADE, related_name="usuario_modificacion_cita", null=True, blank=True)

    def __str__(self):
        return f"Médico: {self.id_medico}, Paciente: {self.id_paciente}, Fecha: {self.fecha_atencion}"
