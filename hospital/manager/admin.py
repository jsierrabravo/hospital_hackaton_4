from django.contrib import admin
from .models import Especialidad, Medico, MedicoEspecialidad, Paciente, Horario, Cita

# Register your models here.
admin.site.register(Especialidad)
admin.site.register(Medico)
admin.site.register(MedicoEspecialidad)
admin.site.register(Paciente)
admin.site.register(Horario)
admin.site.register(Cita)
