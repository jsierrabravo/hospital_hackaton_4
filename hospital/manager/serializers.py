from .models import Especialidad, Medico, MedicoEspecialidad, Paciente, Horario, Cita
from rest_framework import serializers


class EspecialidadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Especialidad
        fields = '__all__'

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['usuario_registro'] = user
        validated_data['usuario_modificacion'] = user
        return Especialidad.objects.create(**validated_data)


class MedicoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Medico
        fields = '__all__'

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['usuario_registro'] = user
        validated_data['usuario_modificacion'] = user
        return Medico.objects.create(**validated_data)


class MedicoEspecialidadSerializer(serializers.ModelSerializer):
    class Meta:
        model = MedicoEspecialidad
        fields = '__all__'

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['usuario_registro'] = user
        validated_data['usuario_modificacion'] = user
        return MedicoEspecialidad.objects.create(**validated_data)


class PacienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Paciente
        fields = '__all__'

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['usuario_registro'] = user
        validated_data['usuario_modificacion'] = user
        return Paciente.objects.create(**validated_data)


class HorarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Horario
        fields = '__all__'

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['usuario_registro'] = user
        validated_data['usuario_modificacion'] = user
        return Horario.objects.create(**validated_data)


class CitaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cita
        fields = '__all__'

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['usuario_registro'] = user
        validated_data['usuario_modificacion'] = user
        return Cita.objects.create(**validated_data)
