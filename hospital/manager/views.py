from django.shortcuts import render
from rest_framework import viewsets
from .models import Especialidad, Medico, MedicoEspecialidad, Paciente, Horario, Cita
from .serializers import EspecialidadSerializer, MedicoSerializer, MedicoEspecialidadSerializer, PacienteSerializer, \
    HorarioSerializer, CitaSerializer
from rest_framework.permissions import IsAuthenticated


# Create your views here.
class EspecialidadViewSet(viewsets.ModelViewSet):
    serializer_class = EspecialidadSerializer
    queryset = Especialidad.objects.all()
    permission_classes = [IsAuthenticated]


class MedicoViewSet(viewsets.ModelViewSet):
    serializer_class = MedicoSerializer
    queryset = Medico.objects.all()
    permission_classes = [IsAuthenticated]


class MedicoEspecialidadViewSet(viewsets.ModelViewSet):
    serializer_class = MedicoEspecialidadSerializer
    queryset = MedicoEspecialidad.objects.all()
    permission_classes = [IsAuthenticated]


class PacienteViewSet(viewsets.ModelViewSet):
    serializer_class = PacienteSerializer
    queryset = Paciente.objects.all()
    permission_classes = [IsAuthenticated]


class HorarioViewSet(viewsets.ModelViewSet):
    serializer_class = HorarioSerializer
    queryset = Horario.objects.all()
    permission_classes = [IsAuthenticated]


class CitaViewSet(viewsets.ModelViewSet):
    serializer_class = CitaSerializer
    queryset = Cita.objects.all()
    permission_classes = [IsAuthenticated]
