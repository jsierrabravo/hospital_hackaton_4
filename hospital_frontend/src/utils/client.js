import axios from 'axios';

export function client(options) {

    let { method, endpoint, handleResponse, values, id } = options;
    
    let url =  `http://127.0.0.1:8000/api/${endpoint}/${id ? `${id}/` : ''}`
    
    let config = {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token"),
        },
    }
 
    switch (method) {
        case 'GET':
            axios.get(url, config)
                .then((response) => handleResponse(response.data))
                .catch((error) => console.log(error))
            break;
        
        case 'POST':
            axios.post(url, values, config)
                .then((response) => console.log(response))
                .catch((error) => console.log(error))
            break;

        case 'PUT':
            axios.put(url, values, config)
                .then((response) => console.log(response))
                .catch((error) => console.log(error))
            break;

        case 'DELETE':
            axios.delete(url, config)
                .then((response) => console.log(response))
                .catch((error) => console.log(error))
            break;

        default:
            console.log(`Sorry, we are out of ${method}`)
            break;
        
    }
}

