import {Form, Button} from 'reactstrap';
import axios from 'axios';
import { client } from '../../utils/client';

export function Especialidad({ id, activo, descripcion, fecha_modificacion, fecha_registro, nombre, usuario_modificacion, usuario_registro }) {

    function handleDelete(id) {

        client({
            method: 'DELETE',
            endpoint: 'especialidades',
            id: id,
        })
    }

    return (
        //<div><h1>Hola episodio</h1></div>
        <tr>
            <th>
                {id}
            </th>
            <td>
                {nombre}
            </td>
            <td>
                {descripcion}
            </td>
            <td>
                {fecha_registro}
            </td>
            {/* <td>
                {usuario_registro}
            </td> */}
            <td>
                {fecha_modificacion}
            </td>
            {/* <td>
                {usuario_modificacion}
            </td> */}
            <td>
                {activo ? 'Sí' : 'No'}
            </td>
            <td>
                <button className="btn btn-danger" onClick={() => handleDelete(id)}>
                    Delete
                </button>
                <a href={`/especialidades/editar/${id}`}>
                    <button className="btn btn-primary">
                        Update
                    </button>
                </a>
                
            </td>
        </tr>
    );
}

