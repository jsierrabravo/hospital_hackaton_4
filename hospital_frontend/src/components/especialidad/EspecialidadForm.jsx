import {Form, FormGroup, Label, Input, Button} from 'reactstrap';
import axios from 'axios';
import {useState, useEffect} from 'react';
import { client } from '../../utils/client';

export function EspecialidadForm({id, metodo}) {

    const initialData = {
        nombre: '', 
        descripcion: '', 
        activo: ''
    }

    const [especialidad, setEspecialidad] = useState(initialData);

    function handleOnChange(event) {
        const {name, value} = event.target;
        // value = value.activo == 'on' ? true : false;
        setEspecialidad({...especialidad, [name]: value})
        if (name=='activo') {
            setEspecialidad({...especialidad, activo: value == 'off' ? false : true})
        }
    }

    function handleSubmit(event) {
        event.preventDefault();
        const formData = new FormData(event.target);
        let values = Object.fromEntries(formData.entries());
        values.activo = values.activo == 'on' ? true : false;
        
        client({
            method: 'POST',
            endpoint: 'especialidades',
            values: values,
        })
    }

    function handleUpdate(event, id) {
        
        especialidad.activo = especialidad.activo == 'on' ? true : false;

        event.preventDefault();
        client({
            method: 'PUT',
            endpoint: 'especialidades',
            values: especialidad,
            id: id,
        })
    }

    if (id){
        
        useEffect(() => {
            client({
                method: 'GET',
                endpoint: 'especialidades',
                id: id,
                handleResponse: setEspecialidad,
            })
        },[]);
        // especialidad.activo = especialidad.activo ? 'on' : 'off';

    } 

    
    return (

        <div>
        {
            especialidad ?
            (
                <Form onSubmit={(e) => {id ? handleUpdate(e, id) : handleSubmit(e)}}>
                    <FormGroup>
                        <Label for="nombre">
                            Nombre
                        </Label>
                        <Input
                            id="nombre"
                            name="nombre"
                            placeholder="Ingrese el nombre de la especialidad"
                            type="text"
                            value={especialidad.nombre}
                            onChange={handleOnChange}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="descripcion">
                            Descripcion
                        </Label>
                        <Input
                            id="descripcion"
                            name="descripcion"
                            type="textarea"
                            value={especialidad.descripcion}
                            onChange={handleOnChange}
                        />
                    </FormGroup>
                    <FormGroup check>
                        <Input
                            type="checkbox" 
                            name="activo"
                            defaultValue={especialidad.activo ? 'on' : 'off'}
                            onChange={handleOnChange}
                        />
                        <Label check>
                            Activo
                        </Label>
                    </FormGroup>
                    <Button>
                        {metodo}
                    </Button>
                </Form>
            ) :
            ""
        }
        </div>

        
    );
}