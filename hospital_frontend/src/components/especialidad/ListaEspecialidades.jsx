import {Especialidad} from './Especialidad';
import {Table} from 'reactstrap';

export function ListaEspecialidades({ especialidades }) {
    
    return (
        <div className="row">
            <Table>
                <thead>
                    <tr>
                        <th>
                            #
                        </th>
                        <th>
                            Nombre
                        </th>
                        <th>
                            Descripción
                        </th>
                        <th>
                            Fecha Registro
                        </th>
                        {/* <th>
                            Usuario Registro
                        </th> */}
                        <th>
                            Fecha Modificación
                        </th>
                        {/* <th>
                            Usuario Modificación
                        </th> */}
                        <th>
                            Activo
                        </th>
                        <th>
                            Acción
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {
                        especialidades.map((especialidad) => {
                            return <Especialidad key={especialidad.id} {...especialidad}/>
                        })
                    }
                    
                </tbody>
            </Table>
                
        </div>
    );
}