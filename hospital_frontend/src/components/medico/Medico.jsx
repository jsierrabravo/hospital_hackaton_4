import {Form, Button} from 'reactstrap';
import axios from 'axios';
import { client } from '../../utils/client';

export function Medico({ id, nombres, apellidos, dni, direccion, correo, telefono, sexo, num_colegiatura, fecha_nacimiento, fecha_registro, fecha_modificacion, activo }) {

    function handleDelete(id) {

        client({
            method: 'DELETE',
            endpoint: 'medicos',
            id: id,
        })
    }

    return (
        //<div><h1>Hola episodio</h1></div>
        <tr>
            <th>
                {id}
            </th>
            <td>
                {nombres}
            </td>
            <td>
                {apellidos}
            </td>
            <td>
                {dni}
            </td>
            <td>
                {direccion}
            </td>
            <td>
                {correo}
            </td>
            <td>
                {telefono}
            </td>
            <td>
                {sexo}
            </td>
            <td>
                {num_colegiatura}
            </td>
            <td>
                {fecha_nacimiento}
            </td>
            <td>
                {fecha_registro}
            </td>
            <td>
                {fecha_modificacion}
            </td>
            <td>
                {activo ? 'Sí' : 'No'}
            </td>
            <td>
                <button className="btn btn-danger" onClick={() => handleDelete(id)}>
                    Delete
                </button>
                <a href={`/medicos/editar/${id}`}>
                    <button className="btn btn-primary">
                        Update
                    </button>
                </a>
                
            </td>
        </tr>
    );
}
