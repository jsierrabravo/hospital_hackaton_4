import {Medico} from './Medico';
import {Table} from 'reactstrap';

export function ListaMedicos({ medicos }) {
    
    return (
        <div className="row">
            <Table>
                <thead>
                    <tr>
                        <th>
                            #
                        </th>
                        <th>
                            Nombres
                        </th>
                        <th>
                            Apellidos
                        </th>
                        <th>
                            DNI
                        </th>
                        <th>
                            Dirección
                        </th>
                        <th>
                            Correo
                        </th>
                        <th>
                            Telefono
                        </th>
                        <th>
                            Sexo
                        </th>
                        <th>
                            Número colegiatura
                        </th>
                        <th>
                            Fecha nacimiento
                        </th>
                        <th>
                            Fecha registro
                        </th>
                        <th>
                            Fecha modificación
                        </th>
                        <th>
                            Activo
                        </th>

                        <th>
                            Acción
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {
                        medicos.map((medico) => {
                            return <Medico key={medico.id} {...medico}/>
                        })
                    }
                    
                </tbody>
            </Table>
                
        </div>
    );
}