import {Form, FormGroup, Label, Input, Button} from 'reactstrap';
import axios from 'axios';
import {useState, useEffect} from 'react';
import { client } from '../../utils/client';

export function MedicoForm({id, metodo}) {

    const initialData = {
        nombres: '', 
        apellidos: '',
        dni: '',
        direccion: '',
        correo: '',
        telefono: '',
        sexo: '',
        num_colegiatura: '',
        fecha_nacimiento: '',
        activo: false,
    }

    const [medico, setMedico] = useState(initialData);

    function handleOnChange(event) {
        const {name, value} = event.target;
        setMedico({...medico, [name]: value});
    }

    function handleSubmit(event) {
        event.preventDefault();
        const formData = new FormData(event.target);
        let values = Object.fromEntries(formData.entries());
        values.activo = values.activo == 'on' ? true : false;
        
        client({
            method: 'POST',
            endpoint: 'medicos',
            values: values,
        })
    }

    function handleUpdate(event, id) {
        
        medico.activo = medico.activo == 'on' ? true : false;

        event.preventDefault();
        client({
            method: 'PUT',
            endpoint: 'medicos',
            values: medico,
            id: id,
        })
    }

    if (id){
        
        useEffect(() => {
            client({
                method: 'GET',
                endpoint: 'medicos',
                id: id,
                handleResponse: setMedico,
            })
        },[]);

    } 
    
    return (

        <div>
        {
            medico ?
            (
                <Form onSubmit={(e) => {id ? handleUpdate(e, id) : handleSubmit(e)}}>
                    <FormGroup>
                        <Label for="nombres">
                            Nombres
                        </Label>
                        <Input
                            id="nombres"
                            name="nombres"
                            placeholder="Ingrese los nombres del médico"
                            type="text"
                            value={medico.nombres}
                            onChange={handleOnChange}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="apellidos">
                            Apellidos
                        </Label>
                        <Input
                            id="apellidos"
                            name="apellidos"
                            placeholder="Ingrese los apellidos del médico"
                            type="text"
                            value={medico.apellidos}
                            onChange={handleOnChange}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="dni">
                            DNI
                        </Label>
                        <Input
                            id="dni"
                            name="dni"
                            type="number"
                            value={medico.dni}
                            onChange={handleOnChange}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="direccion">
                            Dirección
                        </Label>
                        <Input
                            id="direccion"
                            name="direccion"
                            type="text"
                            value={medico.direccion}
                            onChange={handleOnChange}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="correo">
                            Correo
                        </Label>
                        <Input
                            id="correo"
                            name="correo"
                            type="email"
                            value={medico.correo}
                            onChange={handleOnChange}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="telefono">
                            Telefono
                        </Label>
                        <Input
                            id="telefono"
                            name="telefono"
                            type="number"
                            value={medico.telefono}
                            onChange={handleOnChange}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="sexo">
                            Sexo
                        </Label>
                        <Input
                            id="sexo"
                            name="sexo"
                            type="text"
                            value={medico.sexo}
                            onChange={handleOnChange}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="num_colegiatura">
                            No. Colegiatura
                        </Label>
                        <Input
                            id="num_colegiatura"
                            name="num_colegiatura"
                            type="number"
                            value={medico.num_colegiatura}
                            onChange={handleOnChange}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="fecha_nacimiento">
                            Fecha de nacimiento
                        </Label>
                        <Input
                            id="fecha_nacimiento"
                            name="fecha_nacimiento"
                            type="date"
                            value={medico.fecha_nacimiento}
                            onChange={handleOnChange}
                        />
                    </FormGroup>
                    <FormGroup check>
                        <Input
                            type="checkbox" 
                            name="activo"
                            defaultValue={medico.activo ? 'on' : 'off'}
                            onChange={handleOnChange}
                        />
                        <Label check>
                            Activo
                        </Label>
                    </FormGroup>
                    <Button>
                        {metodo}
                    </Button>
                </Form>
            ) :
            ""
        }
        </div>

        
    );
}