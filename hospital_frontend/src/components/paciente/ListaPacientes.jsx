import {Paciente} from './Paciente';
import {Table} from 'reactstrap';

export function ListaPacientes({ pacientes }) {
    
    return (
        <div className="row">
            <Table>
                <thead>
                    <tr>
                    <th>
                            #
                        </th>
                        <th>
                            Nombres
                        </th>
                        <th>
                            Apellidos
                        </th>
                        <th>
                            DNI
                        </th>
                        <th>
                            Dirección
                        </th>
                        <th>
                            Telefono
                        </th>
                        <th>
                            Sexo
                        </th>
                        <th>
                            Fecha nacimiento
                        </th>
                        <th>
                            Fecha registro
                        </th>
                        <th>
                            Fecha modificación
                        </th>
                        <th>
                            Activo
                        </th>

                        <th>
                            Acción
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {
                        pacientes.map((paciente) => {
                            return <Paciente key={paciente.id} {...paciente}/>
                        })
                    }
                    
                </tbody>
            </Table>
                
        </div>
    );
}