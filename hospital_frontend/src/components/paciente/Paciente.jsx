import { client } from '../../utils/client';

export function Paciente({ id, nombres, apellidos, dni, direccion, telefono, sexo, fecha_nacimiento, fecha_registro, fecha_modificacion, activo }) {

    function handleDelete(id) {

        client({
            method: 'DELETE',
            endpoint: 'pacientes',
            id: id,
        })
        
    }

    return (
        //<div><h1>Hola episodio</h1></div>
        <tr>
            <th>
                {id}
            </th>
            <td>
                {nombres}
            </td>
            <td>
                {apellidos}
            </td>
            <td>
                {dni}
            </td>
            <td>
                {direccion}
            </td>
            <td>
                {telefono}
            </td>
            <td>
                {sexo}
            </td>
            <td>
                {fecha_nacimiento}
            </td>
            <td>
                {fecha_registro}
            </td>
            <td>
                {fecha_modificacion}
            </td>
            <td>
                {activo ? 'Sí' : 'No'}
            </td>
            <td>
                <button className="btn btn-danger" onClick={() => handleDelete(id)}>
                    Delete
                </button>
                <a href={`/pacientes/editar/${id}`}>
                    <button className="btn btn-primary">
                        Update
                    </button>
                </a>
                
            </td>
        </tr>
    );
}

