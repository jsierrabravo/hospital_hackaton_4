import {Form, FormGroup, Label, Input, Button} from 'reactstrap';
import {useState, useEffect} from 'react';
import { client } from '../../utils/client';

export function PacienteForm({id, metodo}) {

    const initialData = {
        nombres: '', 
        apellidos: '',
        dni: '',
        direccion: '',
        telefono: '',
        sexo: '',
        fecha_nacimiento: '',
        activo: false,
    }

    const [paciente, setPaciente] = useState(initialData);

    function handleOnChange(event) {
        const {name, value} = event.target;
        setPaciente({...paciente, [name]: value})
    }

    function handleSubmit(event) {
        event.preventDefault();
        const formData = new FormData(event.target);
        let values = Object.fromEntries(formData.entries());
        values.activo = values.activo == 'on' ? true : false;
        
        client({
            method: 'POST',
            endpoint: 'pacientes',
            values: values,
        })
    }

    function handleUpdate(event, id) {
        
        paciente.activo = paciente.activo == 'on' ? true : false;

        event.preventDefault();
        client({
            method: 'PUT',
            endpoint: 'pacientes',
            values: paciente,
            id: id,
        })
    }

    if (id){
        
        useEffect(() => {
            client({
                method: 'GET',
                endpoint: 'pacientes',
                id: id,
                handleResponse: setPaciente,
            })
        },[]);

    } 

    
    return (

        <div>
        {
            paciente ?
            (
                <Form onSubmit={(e) => {id ? handleUpdate(e, id) : handleSubmit(e)}}>
                    <FormGroup>
                        <Label for="nombres">
                            Nombres
                        </Label>
                        <Input
                            id="nombres"
                            name="nombres"
                            placeholder="Ingrese los nombres del médico"
                            type="text"
                            value={paciente.nombres}
                            onChange={handleOnChange}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="apellidos">
                            Apellidos
                        </Label>
                        <Input
                            id="apellidos"
                            name="apellidos"
                            placeholder="Ingrese los apellidos del médico"
                            type="text"
                            value={paciente.apellidos}
                            onChange={handleOnChange}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="dni">
                            DNI
                        </Label>
                        <Input
                            id="dni"
                            name="dni"
                            type="number"
                            value={paciente.dni}
                            onChange={handleOnChange}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="direccion">
                            Dirección
                        </Label>
                        <Input
                            id="direccion"
                            name="direccion"
                            type="text"
                            value={paciente.direccion}
                            onChange={handleOnChange}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="telefono">
                            Telefono
                        </Label>
                        <Input
                            id="telefono"
                            name="telefono"
                            type="number"
                            value={paciente.telefono}
                            onChange={handleOnChange}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="sexo">
                            Sexo
                        </Label>
                        <Input
                            id="sexo"
                            name="sexo"
                            type="text"
                            value={paciente.sexo}
                            onChange={handleOnChange}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="fecha_nacimiento">
                            Fecha de nacimiento
                        </Label>
                        <Input
                            id="fecha_nacimiento"
                            name="fecha_nacimiento"
                            type="date"
                            value={paciente.fecha_nacimiento}
                            onChange={handleOnChange}
                        />
                    </FormGroup>
                    <FormGroup check>
                        <Input
                            type="checkbox" 
                            name="activo"
                            defaultValue={paciente.activo ? 'on' : 'off'}
                            onChange={handleOnChange}
                        />
                        <Label check>
                            Activo
                        </Label>
                    </FormGroup>
                    <Button>
                        {metodo}
                    </Button>
                </Form>
            ) :
            ""
        }
        </div>

        
    );
}