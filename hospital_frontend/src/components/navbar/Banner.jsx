import bannerImg from '../../assets/img/banner-img.jpg';
export function Banner() {
    return (
        <div className="card bg-dark text-white">
            <img src={bannerImg} className="card-img" alt="banner"/>
        </div>
    );
}