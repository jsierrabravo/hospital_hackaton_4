import { Link } from "react-router-dom";
export function Header() {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container-fluid">
            <Link className="navbar-brand" to="#">
                MI HOSPITAL
            </Link>
            <div className="collapse navbar-collapse" id="navbarText">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                    <Link className="nav-link active" aria-current="page" to="/">Home</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link active" aria-current="page" to="/especialidades/">Especialidades</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link active" aria-current="page" to="/medicos/">Médicos</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link active" aria-current="page" to="/pacientes/">Pacientes</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link active" aria-current="page" to="/citas/">Citas</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link active" aria-current="page" to="/login">Login</Link>
                </li>
            </ul>
            </div>
        </div>
        </nav>
    );
}