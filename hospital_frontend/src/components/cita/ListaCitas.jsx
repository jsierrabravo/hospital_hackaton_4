import {Cita} from './Cita';
import {Table} from 'reactstrap';

export function ListaCitas({ citas }) {
    
    return (
        <div className="row">
            <Table>
                <thead>
                    <tr>
                        <th>
                            #
                        </th>
                        <th>
                            Fecha atención
                        </th>
                        <th>
                            Inicio atención
                        </th>
                        <th>
                            Fin atención
                        </th>
                        <th>
                            Estado
                        </th>
                        <th>
                            Observaciones
                        </th>
                        <th>
                            Activo
                        </th>
                        <th>
                            Fecha registro
                        </th>
                        <th>
                            Fecha modificación
                        </th>
                        <th>
                            Médico
                        </th>
                        <th>
                            Paciente
                        </th>
                        <th>
                            Acción
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {
                        citas.map((cita) => {
                            return <Cita key={cita.id} {...cita}/>
                        })
                    }
                    
                </tbody>
            </Table>
                
        </div>
    );
}