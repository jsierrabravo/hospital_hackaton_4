import {Form, FormGroup, Label, Input, Button} from 'reactstrap';
import {useState, useEffect} from 'react';
import { client } from '../../utils/client';

export function CitaForm({id, metodo}) {

    const initialData = {
        fecha_atencion: '', 
        inicio_atencion: '',
        fin_atencion: '',
        estado: false,
        observaciones: '',
        activo: false,
        id_medico: 0,
        id_paciente: 0,
    }

    const initialDataMedicos = {
        nombres: '', 
        apellidos: '',
        dni: '',
        direccion: '',
        correo: '',
        telefono: '',
        sexo: '',
        num_colegiatura: '',
        fecha_nacimiento: '',
        activo: false,
    }

    const initialDataPacientes = {
        nombres: '', 
        apellidos: '',
        dni: '',
        direccion: '',
        telefono: '',
        sexo: '',
        fecha_nacimiento: '',
        activo: false,
    }

    const [cita, setCita] = useState(initialData);
    const [medicos, setMedicos] = useState([initialDataMedicos]);
    const [pacientes, setpacientes] = useState([initialDataPacientes]);

    console.log(medicos);

    useEffect(() => {
        client({
            method: 'GET',
            endpoint: 'medicos',
            handleResponse: setMedicos,
        })
    },[]);

    useEffect(() => {
        client({
            method: 'GET',
            endpoint: 'pacientes',
            handleResponse: setpacientes,
        })
    },[]);

    useEffect(() => {
        client({
            method: 'GET',
            endpoint: 'citas',
            id: id,
            handleResponse: setCita,
        })
    },[]);

    function handleOnChange(event) {
        const {name, value} = event.target;
        setCita({...cita, [name]: value})
    }

    function handleSubmit(event) {
        event.preventDefault();
        const formData = new FormData(event.target);
        let values = Object.fromEntries(formData.entries());
        values.activo = values.activo == 'on' ? true : false;
        values.estado = values.estado == 'on' ? true : false;
        
        client({
            method: 'POST',
            endpoint: 'citas',
            values: values,
        })
    }

    function handleUpdate(event, id) {
        
        cita.activo = cita.activo == 'on' ? true : false;
        cita.estado = cita.estado == 'on' ? true : false;

        event.preventDefault();
        client({
            method: 'PUT',
            endpoint: 'citas',
            values: cita,
            id: id,
        })
    }

    if (id){
        
        useEffect(() => {
            client({
                method: 'GET',
                endpoint: 'citas',
                id: id,
                handleResponse: setCita,
            })
        },[]);

    } 

    return (

        <div>
        {
            cita ?
            (
                <Form onSubmit={(e) => {id ? handleUpdate(e, id) : handleSubmit(e)}}>
                    <FormGroup>
                        <Label for="fecha_atencion">
                            Fecha atencion
                        </Label>
                        <Input
                            id="fecha_atencion"
                            name="fecha_atencion"
                            type="date"
                            value={cita.fecha_atencion}
                            onChange={handleOnChange}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="inicio_atencion">
                            Inicio atencion
                        </Label>
                        <Input
                            id="inicio_atencion"
                            name="inicio_atencion"
                            type="date"
                            value={cita.inicio_atencion}
                            onChange={handleOnChange}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="fin_atencion">
                            Fin atencion
                        </Label>
                        <Input
                            id="fin_atencion"
                            name="fin_atencion"
                            type="date"
                            value={cita.fin_atencion}
                            onChange={handleOnChange}
                        />
                    </FormGroup>
                    <FormGroup check>
                        <Input
                            type="checkbox" 
                            name="estado"
                            defaultValue={cita.estado ? 'on' : 'off'}
                            onChange={handleOnChange}
                        />
                        <Label check>
                            Estado
                        </Label>
                    </FormGroup>
                    <FormGroup>
                        <Label for="observaciones">
                            Observaciones
                        </Label>
                        <Input
                            id="observaciones"
                            name="observaciones"
                            type="textarea"
                            value={cita.observaciones}
                            onChange={handleOnChange}
                        />
                    </FormGroup>
                    <FormGroup check>
                        <Input
                            type="checkbox" 
                            name="activo"
                            defaultValue={cita.activo ? 'on' : 'off'}
                            onChange={handleOnChange}
                        />
                        <Label check>
                            Activo
                        </Label>
                    </FormGroup>
                    <FormGroup>
                        <Label for="id_medico">
                            Medico
                        </Label>
                        <Input
                            id="id_medico"
                            name="id_medico"
                            className="mb-3"
                            type="select"
                            onChange={handleOnChange}
                        >
                            <option defaultValue="None">--Seleccione un médico--</option>
                            {
                                medicos.results?.map((medico) => {
                                    return (
                                        <option key={medico.id} value={medico.id} name="id_medico">
                                            {`${medico.nombres} ${medico.apellidos}`}
                                        </option>
                                    );
                                })
                            }
                        </Input>
                    </FormGroup>
                    <FormGroup>
                        <Label for="id_paciente">
                            Paciente
                        </Label>
                        <Input
                            id="id_paciente"
                            name="id_paciente"
                            className="mb-3"
                            type="select"
                            onChange={handleOnChange}
                        >
                            <option defaultValue="None">--Seleccione un paciente--</option>
                            {
                                pacientes.results?.map((paciente) => {
                                    return (
                                        <option key={paciente.id} value={paciente.id} name="id_paciente">
                                            {`${paciente.nombres} ${paciente.apellidos}`}
                                        </option>
                                    );
                                })
                            }
                        </Input>
                    </FormGroup>
                    <Button>
                        {metodo}
                    </Button>
                </Form>
            ) :
            ""
        }
        </div>

        
    );
}