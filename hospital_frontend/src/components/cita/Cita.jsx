import { client } from '../../utils/client';
import {useState, useEffect} from 'react';

export function Cita({ id, fecha_atencion, inicio_atencion, fin_atencion, estado, observaciones, activo, fecha_registro, fecha_modificacion, id_medico, id_paciente }) {

    const [medico, setMedico] = useState({nombres:'', apellidos: ''});
    const [paciente, setPaciente] = useState({nombres:'', apellidos: ''});

    useEffect(() => {
        client({
            method: 'GET',
            endpoint: 'medicos',
            id: id_medico,
            handleResponse: setMedico,
        })
    },[]);

    useEffect(() => {
        client({
            method: 'GET',
            endpoint: 'pacientes',
            id: id_paciente,
            handleResponse: setPaciente,
        })
    },[]);

    function handleDelete(id) {

        client({
            method: 'DELETE',
            endpoint: 'citas',
            id: id,
        })
    }

    return (
        <tr>
            <th>
                {id}
            </th>
            <td>
                {fecha_atencion}
            </td>
            <td>
                {inicio_atencion}
            </td>
            <td>
                {fin_atencion}
            </td>
            <td>
                {estado}
            </td>
            <td>
                {observaciones}
            </td>
            <td>
                {activo ? 'Sí' : 'No'}
            </td>
            <td>
                {fecha_registro}
            </td>
            <td>
                {fecha_modificacion}
            </td>
            <td>
                {`${medico.nombres} ${medico.apellidos}`}
            </td>
            <td>
                {`${paciente.nombres} ${paciente.apellidos}`}
            </td>
            <td>
                <button className="btn btn-danger" onClick={() => handleDelete(id)}>
                    Delete
                </button>
                <a href={`/citas/editar/${id}`}>
                    <button className="btn btn-primary">
                        Update
                    </button>
                </a>
                
            </td>
        </tr>
    );
}


