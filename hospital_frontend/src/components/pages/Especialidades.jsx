import {useState, useEffect} from 'react';
import {ListaEspecialidades} from '../especialidad/ListaEspecialidades';
import { client } from '../../utils/client';

export function Especialidades() {
    const [especialidades, setEspecialidades] = useState(null)

    const getEspecialidades = () => {
        client({
            method: 'GET',
            endpoint: 'especialidades',
            handleResponse: setEspecialidades,
        })
    }

    useEffect(() => {
        getEspecialidades();
    },[]);

    return (
        <div>

            <div className="px-5">

                <h1 className="py-5">Especialidades</h1>

                <div className="d-grid gap-2 d-md-flex justify-content-md-end mb-5">
                    <a href="/especialidades/agregar">
                        <button className="btn btn-primary me-md-2" type="button">Agregar especialidad</button>
                    </a>
                </div>
                {especialidades ? <ListaEspecialidades especialidades={especialidades.results}/> : <div>Cargando especialidades...</div>}

            </div>

            
        </div>
    );
}