import { PacienteForm } from '../paciente/PacienteForm';
import {useParams} from 'react-router-dom';


export function PacientesForm({ pacientes, metodo}) {
    
    let {id} = useParams()

    return (
        <div>
            <h3>{`${metodo} paciente`}</h3>
            
            <div className="container">
                <PacienteForm id={id} metodo={metodo} />    
            </div>
        </div>
    );
}