import {useState, useEffect} from 'react';
import {ListaCitas} from '../cita/ListaCitas';
import { client } from '../../utils/client';

export function Citas() {
    const [citas, setCitas] = useState(null)

    const getCitas = () => {
        client({
            method: 'GET',
            endpoint: 'citas',
            handleResponse: setCitas,
        })
    }

    useEffect(() => {
        getCitas();
    },[]);

    return (
        <div>

            <div className="px-5">

                <h1 className="py-5">Citas agendadas</h1>

                <div className="d-grid gap-2 d-md-flex justify-content-md-end mb-5">
                    <a href="/citas/agregar">
                        <button className="btn btn-primary me-md-2" type="button">Agregar cita</button>
                    </a>
                </div>
                {citas ? <ListaCitas citas={citas.results}/> : <div>Cargando citas...</div>}

            </div>

            
        </div>
    );
}