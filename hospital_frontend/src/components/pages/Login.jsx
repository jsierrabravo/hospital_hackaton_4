import React, { useRef, useContext, useEffect, useState } from "react";
import {Form, FormGroup, Label, Input, FormText, Button, Col} from 'reactstrap';
import { useDispatch, useSelector } from "react-redux";
import axios from 'axios';
import { useNavigate } from "react-router-dom";


export function Login () {

    const loginValid = useNavigate();

    const login = (values) => {
        console.log(values);
    
        const urlToken = "http://127.0.0.1:8000/api/token/";
        axios
          .post(urlToken, {
            username: values.username,
            password: values.password,
          })
          .then((response) => {
            localStorage.setItem("token", response.data.access);
            loginValid('/');
          })
          .catch((error) => {
            console.log(error);
          });
      };

    const submit = (e) => {
        e.preventDefault();
        const formData = new FormData(e.target);
        let values = Object.fromEntries(formData.entries());
        console.log(values);
        login(values);
    }

    return (
        <div className="container">
            <h1>Login</h1>
            <Form
                onSubmit={(e) => submit(e)}
            >
                <FormGroup>
                    <Label for="username">
                        Username
                    </Label>
                    <Input
                        id="username"
                        name="username"
                        placeholder="Type your username"
                        type="text"
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="password">
                        Password
                    </Label>
                    <Input
                        id="password"
                        name="password"
                        placeholder="Password"
                        type="password"
                    />
                </FormGroup>
                
                <Button>
                    Submit
                </Button>
            </Form>
        </div>
    );
}