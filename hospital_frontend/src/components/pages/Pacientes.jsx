import {useState, useEffect} from 'react';
import {ListaPacientes} from '../paciente/ListaPacientes';
import { client } from '../../utils/client';

export function Pacientes() {
    const [pacientes, setPacientes] = useState(null)

    const getPacientes = () => {
        client({
            method: 'GET',
            endpoint: 'pacientes',
            handleResponse: setPacientes,
        })
    }

    useEffect(() => {
        getPacientes();
    },[]);

    return (
        <div>

            <div className="px-5">

                <h1 className="py-5">Pacientes</h1>

                <div className="d-grid gap-2 d-md-flex justify-content-md-end mb-5">
                    <a href="/pacientes/agregar">
                        <button className="btn btn-primary me-md-2" type="button">Agregar paciente</button>
                    </a>
                </div>
                {pacientes ? <ListaPacientes pacientes={pacientes.results}/> : <div>Cargando pacientes...</div>}

            </div>

            
        </div>
    );
}