import {useState, useEffect} from 'react';
import {ListaMedicos} from '../medico/ListaMedicos';
import { client } from '../../utils/client';

export function Medicos() {
    const [medicos, setMedicos] = useState(null)

    const getMedicos = () => {
        client({
            method: 'GET',
            endpoint: 'medicos',
            handleResponse: setMedicos,
        })
    }

    useEffect(() => {
        getMedicos();
    },[]);

    return (
        <div>

            <div className="px-5">

                <h1 className="py-5">Medicos</h1>

                <div className="d-grid gap-2 d-md-flex justify-content-md-end mb-5">
                    <a href="/medicos/agregar">
                        <button className="btn btn-primary me-md-2" type="button">Agregar medico</button>
                    </a>
                </div>
                {medicos ? <ListaMedicos medicos={medicos.results}/> : <div>Cargando medicos...</div>}

            </div>

            
        </div>
    );
}