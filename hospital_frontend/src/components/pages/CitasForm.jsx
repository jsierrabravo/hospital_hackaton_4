import { CitaForm } from '../cita/CitaForm';
import {useParams} from 'react-router-dom';


export function CitasForm({ citas, metodo}) {
    
    let {id} = useParams()

    return (
        <div>
            <h3>{`${metodo} cita`}</h3>
            
            <div className="container">
                <CitaForm id={id} metodo={metodo} />    
            </div>
        </div>
    );
}