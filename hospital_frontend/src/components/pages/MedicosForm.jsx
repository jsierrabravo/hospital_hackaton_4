import { MedicoForm } from '../medico/MedicoForm';
import {useParams} from 'react-router-dom';


export function MedicosForm({ medicos, metodo}) {
    
    let {id} = useParams()

    return (
        <div>
            <h3>{`${metodo} médico`}</h3>
            
            <div className="container">
                <MedicoForm id={id} metodo={metodo} />    
            </div>
        </div>
    );
}