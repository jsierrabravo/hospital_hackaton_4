import { EspecialidadForm } from '../especialidad/EspecialidadForm';
import {useParams} from 'react-router-dom';


export function EspecialidadesForm({ especialidades, metodo}) {
    
    let {id} = useParams()

    return (
        <div>
            <h3>{`${metodo} especialidad`}</h3>
            
            <div className="container">
                <EspecialidadForm id={id} metodo={metodo} />    
            </div>
        </div>
    );
}