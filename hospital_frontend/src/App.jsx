import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Routes, Route } from "react-router-dom";

import {Home} from "./components/pages/Home";
import {Header} from "./components/navbar/Header";
import {Footer} from "./components/footer/Footer";
import {Banner} from './components/navbar/Banner';
import {Login} from "./components/pages/Login";

import {Especialidades} from "./components/pages/Especialidades";
import {EspecialidadesForm} from "./components/pages/EspecialidadesForm";

import {Medicos} from "./components/pages/Medicos";
import {MedicosForm} from "./components/pages/MedicosForm";

import {Pacientes} from "./components/pages/Pacientes";
import {PacientesForm} from "./components/pages/PacientesForm";

import {Citas} from "./components/pages/Citas";
import {CitasForm} from "./components/pages/CitasForm";

function App() {
  
  return (
    <div className="App d-flex flex-column min-vh-100">
      <Header/>
      <Banner/>

      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/login" element={<Login />}/>

        <Route path="/especialidades" element={<Especialidades />} />
        <Route path="/especialidades/agregar" element={<EspecialidadesForm metodo="Agregar" />} />
        <Route path="/especialidades/editar/:id" element={<EspecialidadesForm metodo="Editar" />} />

        <Route path="/medicos" element={<Medicos />} />
        <Route path="/medicos/agregar" element={<MedicosForm metodo="Agregar" />} />
        <Route path="/medicos/editar/:id" element={<MedicosForm metodo="Editar" />} />

        <Route path="/pacientes" element={<Pacientes />} />
        <Route path="/pacientes/agregar" element={<PacientesForm metodo="Agregar" />} />
        <Route path="/pacientes/editar/:id" element={<PacientesForm metodo="Editar" />} /> 

        <Route path="/citas" element={<Citas />} />
        <Route path="/citas/agregar" element={<CitasForm metodo="Agregar" />} />
        <Route path="/citas/editar/:id" element={<CitasForm metodo="Editar" />} />

      </Routes>

    <Footer/>
    </div>
  )
}

export default App
